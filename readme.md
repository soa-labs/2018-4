# SOA labs: 5. Introduce API Gateway
## Use Feign Client for consuming microservices

### Reproduce steps

1. Clone the project
   * `git clone git@gitlab.com:soa-labs/2018-4.git`

2. Build all docker images:
   * `./build-images.sh`

3. Start mysql (and wait for it to start):
   * `docker-compose up -d mysql`

4. Ensure mysql container is up and running:
   * `docker ps`
   * `docker logs <container-id>` 

5. Start other services (and wait for them to run):
   * `docker-compose up -d`
   * `docker ps` 

6. Scale app1 and app2:
   * `docker-compose scale app1=2 app2=2`

6. See eureka in web browser:
   * `http://localhost/eureka`

7. Access API Gateway aggregated greeting endpoint:
   * `http://localhost/zuul/gateway/greeting`

9. Shutdown containers:
   * `docker-compose down`
